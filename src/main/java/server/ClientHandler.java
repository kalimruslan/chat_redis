package server;

import models.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by 1 on 10.07.2017.
 */
class ClientHandler extends Thread {

    private Server server;
    private Socket clientSock;
    private BufferedReader bufferedReader;
    private PrintWriter printWriter;
    private String login;
    private String reseipent;

    public ClientHandler(Server server, Socket sock) throws IOException {
        this.server = server;
        clientSock = sock;
        bufferedReader = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        printWriter = new PrintWriter(sock.getOutputStream(), true);
    }

    public ClientHandler() {

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void run() {
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                char c = line.charAt(0);
                line = line.substring(1);
                switch (c) {
                    case Utils.COMMAND_AUTH:
                        if (!Utils.isValidLoginName(line)) {
                            send(true, Utils.SERVER_NAME, "Ник " + line + " неверный");
                            continue;
                        }

                        login = line;

                        synchronized (server.getClientList()) {
                            server.getClientList().add(this);
                            if (server.getClientList().size() == 1)
                                send(true, Utils.SERVER_NAME, "Добро пожаловать! вы первый в чате.");
                            else {
                                send(true, Utils.SERVER_NAME, "Кроме вас в чате " +
                                        (server.getClientList().size() - 1) + " пользователей.");
                            }
                        }
                        break;

                    case Utils.COMMAND_RECEIPENT:
                        reseipent = line;
                        break;

                    case Utils.COMMAND_PRIVATE_MESSAGE:
                        server.log("MESG: " + login + "-->" + reseipent + ": " + line);
                        ClientHandler cl = findClient(reseipent);
                        if (cl == null)
                            sendPrivate(reseipent + " не авторизован.");
                        else
                            cl.send(false, login, line);
                        break;

                    case Utils.COMMAND_QUIT:
                        sendToAll(true, Utils.SERVER_NAME,
                                "До свидания, " + login);
                        close();
                        return;

                    case Utils.COMMAND_TOTAL_MESSAGE:
                        if (login != null)
                            sendToAll(false, login, line);
                        break;
                }
            }
        } catch (IOException e) {
            server.log("IO Exception: " + e);
        } finally {
            System.out.println("Произошла ошибка");
            synchronized (server.getClientList()) {
                server.getClientList().remove(this);
            }
        }
    }

    private void close() {
        if (clientSock == null) {
            return;
        }
        try {
            clientSock.close();
            clientSock = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void send(boolean msgFromServer, String sender, String mesg) {
        if (msgFromServer) {
            printWriter.println(Utils.ANSI_GREEN + sender + Utils.SEPARATOR + mesg + Utils.ANSI_RESET);
        } else {
            printWriter.println(sender + Utils.SEPARATOR + mesg);
        }
    }

    private void sendPrivate(String msg) {
        send(true, Utils.SERVER_NAME, msg);
    }

    private void sendToAll(boolean msgFromServer, String sender, String mesg) {
        System.out.println(Utils.ANSI_GREEN + "ДЛЯ ВСЕХ " + sender + Utils.SEPARATOR + mesg + Utils.ANSI_RESET);
        for (int i = 0; i < server.getClientList().size(); i++) {
            ClientHandler сlientHandler = (ClientHandler) server.getClientList().get(i);
            сlientHandler.send(msgFromServer, sender, mesg);
        }
    }

    private ClientHandler findClient(String nick) {
        synchronized (server.getClientList()) {
            for (int i = 0; i < server.getClientList().size(); i++) {
                ClientHandler сlientHandler = (ClientHandler) server.getClientList().get(i);
                if (сlientHandler.login.equals(nick))
                    return сlientHandler;
            }
        }
        return null;
    }
}
