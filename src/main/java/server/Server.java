package server;

import models.Config;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

class Server {
    private ServerSocket serverSocket;
    private final ArrayList clientsList;
    private static Config settings;

    public List getClientList() {
        return clientsList;
    }

    private Server() {
        clientsList = new ArrayList();
        try {
            serverSocket = new ServerSocket(Integer.parseInt(settings.getSocketServerPort()));
            System.out.println("Сервер слушает порт " + settings.getSocketServerPort());
        } catch (IOException e) {
            log("Произошла ошибка на сервере");
            System.exit(0);
        }
    }

    public static void main(String[] argv) {
        Config.createConfigFile();
        settings = Config.createInstance();
        Config.readConfigFile(settings);

        System.out.println("**********************************************");
        System.out.println("\t\tПараметры конфигурационного файла:");
        System.out.println(settings.getAllSettings());
        System.out.println("**********************************************");

        System.out.println("Запуск сервера...");
        Server server = new Server();
        server.runServer();
    }

    private void runServer() {
        try {
            while (true) {
                Socket socket = serverSocket.accept();
                ClientHandler сlientHandler = new ClientHandler(this, socket);
                сlientHandler.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
            log("Ошибка ввода-вывода");
            System.exit(0);
        }
    }

    void log(String s) {
        System.out.println(s);
    }

}
