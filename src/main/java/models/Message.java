package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by 1 on 14.07.2017.
 */
public class Message implements Comparable {

    private String sender;
    private String receipent;
    private String message;
    private String date;

    public Message(String sender, String receipent) {
        this.sender = sender;
        this.receipent = receipent;
    }

    public Message(String sender, String receipent, String message) {
        this.sender = sender;
        this.receipent = receipent;
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSender() {
        return sender;
    }

    public String getReceipent() {
        return receipent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int compareTo(Object o) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        try {
            return sdf.parse(this.date).compareTo(sdf.parse(((Message) o).getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
