package models;

/**
 * Created by 1 on 10.07.2017.
 */
public class Help {
    public static void showHelp() {
        System.out.println("*****************************************************");
        StringBuilder sb = new StringBuilder("\t\t\tКоманды для чата: \n");
        sb
                .append("HELP - вызов помощника по чату,\n")
                .append("USERS - просмотреть список всех пользователей,\n")
                .append("CONFIG - просмотреть параметры сервера,\n")
                .append("HISTORY - посмотреть историю входящ и исходящих сообщений,\n")
                .append("PRIVATE - отправить сообщение конкретному пользователю\n")
                .append("ALL - отправить сообщение всем пользователям\n")
                .append("QUIT - выйти из чата");
        System.out.println(sb);
        System.out.println("*****************************************************");
    }
}
