package models;

/**
 * Created by 1 on 04.07.2017.
 */
public class Utils {
    public final static String SERVER_NAME = "Сервер";
    public final static String SEPARATOR = ": ";

    public static final String FILE_PATH_CONFIG = "src/configs/config.xml";

    public static final String MSG_SUFFIX_IN = "_in";
    public static final String MSG_SUFFIX_OUT = "_out";

    public static final String REDIS_USER_LIST = "all_users_list";

    public static final int PORT = 9999;
    public static final int JEDIS_PORT = 6379;
    private static final int MAX_LOGIN_LENGTH = 20;
    public static final char COMMAND_AUTH = 'L';
    public static final char COMMAND_QUIT = 'Q';
    public static final char COMMAND_PRIVATE_MESSAGE = 'M';
    public static final char COMMAND_TOTAL_MESSAGE = 'B';
    public static final char COMMAND_RECEIPENT = 'R';

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_CYAN = "\u001B[36m";

    public static boolean isValidLoginName(String login) {
        // check length
        return login.length() <= MAX_LOGIN_LENGTH;
    }


}
