package models;

import javax.xml.stream.*;
import java.io.*;

/**
 * Created by 1 on 10.07.2017.
 */
public class Config {

    private static Config instance;

    private Config() {
    }

    public static Config createInstance() {
        if (instance == null)
            instance = new Config();
        return instance;
    }

    private String socketServerHost;
    private String socketServerPort;
    private String jedisServerHost;
    private String jedisServerPost;

    public String getSocketServerHost() {
        return socketServerHost;
    }

    private void setSocketServerHost(String socketServerHost) {
        this.socketServerHost = socketServerHost;
    }

    public String getSocketServerPort() {
        return socketServerPort;
    }

    private void setSocketServerPort(String socketServerPort) {
        this.socketServerPort = socketServerPort;
    }

    public String getJedisServerHost() {
        return jedisServerHost;
    }

    private void setJedisServerHost(String jedisServerHost) {
        this.jedisServerHost = jedisServerHost;
    }

    public String getJedisServerPost() {
        return jedisServerPost;
    }

    private void setJedisServerPost(String jedisServerPost) {
        this.jedisServerPost = jedisServerPost;
    }

    public static void createConfigFile() {
        File file = new File(Utils.FILE_PATH_CONFIG);
        if (!file.exists()) {
            XMLOutputFactory outFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = null;
            try {
                writer = outFactory.createXMLStreamWriter(new FileWriter(Utils.FILE_PATH_CONFIG));
            } catch (XMLStreamException | IOException e) {
                e.printStackTrace();
            }

            try {
                writer.writeStartDocument();

                writer.writeStartElement("ServerSettings");

                writer.writeStartElement("socketHost");
                writer.writeCharacters("localhost");
                writer.writeEndElement();

                writer.writeStartElement("socketPort");
                writer.writeCharacters(Integer.toString(Utils.PORT));
                writer.writeEndElement();

                writer.writeStartElement("jedisHost");
                writer.writeCharacters("localhost");
                writer.writeEndElement();

                writer.writeStartElement("jedisPort");
                writer.writeCharacters(Integer.toString(Utils.JEDIS_PORT));
                writer.writeEndElement();

                writer.writeEndElement();
                writer.writeEndDocument();

                writer.flush();
                writer.close();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
        }
    }

    public static void readConfigFile(Config settings) {
        String text = null;

        XMLInputFactory inFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader =
                null;
        try {
            reader = inFactory.createXMLStreamReader(new FileInputStream(new File(Utils.FILE_PATH_CONFIG)));
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while (reader.hasNext()) {
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.CHARACTERS: {
                        text = reader.getText().trim();
                        break;
                    }
                    case XMLStreamConstants.END_ELEMENT: {
                        switch (reader.getLocalName()) {
                            case "socketHost":
                                settings.setSocketServerHost(text);
                                break;
                            case "socketPort":
                                settings.setSocketServerPort(text);
                                break;
                            case "jedisHost":
                                settings.setJedisServerHost(text);
                                break;
                            case "jedisPort":
                                settings.setJedisServerPost(text);
                        }
                        break;
                    }
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public String getAllSettings() {
        return "Хост для сервера сокетов: " + this.getSocketServerHost() + "\n"
                + "Порт для сервера сокетов: " + this.getSocketServerPort() + "\n"
                + "Хост для Редис: " + this.getJedisServerHost() + "\n"
                + "Порт для Редис: " + this.getJedisServerPost();
    }
}
