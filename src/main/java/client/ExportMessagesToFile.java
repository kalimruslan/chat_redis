package client;

import models.Message;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.util.List;

/**
 * Created by 1 on 14.07.2017.
 */
class ExportMessagesToFile {
    private final List<Message> allHistoriesIn;
    private final List<Message> allHistoriesOut;
    private final String nikc;
    private String filePath;

    public ExportMessagesToFile(String nikc, List<Message> allHistoriesIn, List<Message> allHistoriesOut) {
        this.allHistoriesIn = allHistoriesIn;
        this.allHistoriesOut = allHistoriesOut;
        this.nikc = nikc;
    }

    public void writeToJSON() {
        try {
            filePath = nikc + "_histories.json";
            JSONObject jsonObject = new JSONObject();

            JSONArray jsonArrayIn = new JSONArray();
            JSONArray jsonArrayOut = new JSONArray();

            for (Message mess : allHistoriesIn) {
                JSONObject jsonMessageObject = new JSONObject();
                jsonMessageObject.put("Date", mess.getDate());
                jsonMessageObject.put("From", mess.getSender());
                jsonMessageObject.put("Message", mess.getMessage());

                jsonArrayIn.add(jsonMessageObject);
            }

            for (Message mess : allHistoriesOut) {
                JSONObject jsonMessageObject = new JSONObject();
                jsonMessageObject.put("Date", mess.getDate());
                jsonMessageObject.put("To", mess.getReceipent());
                jsonMessageObject.put("Message", mess.getMessage());

                jsonArrayOut.add(jsonMessageObject);
            }

            jsonObject.put("Incoming", jsonArrayIn);
            jsonObject.put("Outgoing", jsonArrayOut);

            FileWriter fileWriter = new FileWriter(filePath);

            fileWriter.write(jsonObject.toJSONString());
            fileWriter.close();

            System.out.println("Файл успешно сохранен по пути: " + filePath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
