package client;

import com.google.gson.Gson;
import models.*;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 1 on 04.07.2017.
 */
@SuppressWarnings("WeakerAccess")
class Clients {

    public static void main(String[] args) throws IOException {
        new Clients().chatting();
    }

    private Socket sock;
    private BufferedReader is;
    private PrintWriter pw;
    private BufferedReader cons;
    private Jedis jedis;
    private String receipent;
    private String nikc;

    Clients() throws IOException {
        Config settings = Config.createInstance();
        Config.readConfigFile(settings);

        sock = new Socket(settings.getSocketServerHost(), Integer.parseInt(settings.getSocketServerPort()));
        is = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        pw = new PrintWriter(sock.getOutputStream(), true);
        cons = new BufferedReader(new InputStreamReader(System.in));

        jedis = new Jedis(settings.getJedisServerHost(), Integer.parseInt(settings.getJedisServerPost()));

        new Thread(() -> {
            System.out.flush();
            String line;
            try {
                while ((line = is.readLine()) != null) {
                    System.out.println(line);
                    System.out.flush();
                }
            } catch (IOException ex) {
                System.err.println("Read error on socket: " + ex);
            }
        }).start();
    }

    private void chatting() throws IOException {
        System.out.println(Utils.ANSI_CYAN + "Для начала общения необходимо авторизоваться. Введите Ваш ник: "
                + Utils.ANSI_RESET);
        System.out.flush();
        nikc = cons.readLine();
        send(Utils.COMMAND_AUTH + nikc);
        Users users = new Users(nikc);
        if (!checkIfHasUser(users))
            addNewUser(users);

        System.out.println("Для вызова помощника введите HELP");

        while (true) {
            System.out.println(Utils.ANSI_BLUE + "Введите команду(или команду HELP):" + Utils.ANSI_RESET);
            String command;
            command = cons.readLine();

            switch (command.toUpperCase()) {
                case "HELP":
                    Help.showHelp();
                    break;

                case "USERS":
                    showAllUsers();
                    break;

                case "ALL":
                    handleMessage(Utils.COMMAND_TOTAL_MESSAGE);
                    break;

                case "HISTORY":
                    handleHistoryCommand();
                    break;

                case "PRIVATE": {
                    System.out.println("Введите получателя: ");
                    receipent = cons.readLine();
                    send(Utils.COMMAND_RECEIPENT + receipent);

                    handleMessage(Utils.COMMAND_PRIVATE_MESSAGE);
                    break;
                }
                case "QUIT":
                    send(Utils.COMMAND_QUIT + "");
                default:
                    System.out.println(Utils.ANSI_RED + "Неправильная команда, введите заново" + Utils.ANSI_RESET);
                    System.out.println();
            }
        }
    }

    private void handleHistoryCommand() throws IOException {
        List<Message> allHistoriesOut = getAllHistoriesOut();
        List<Message> allHistoriesIn = getAllHistoriesIn();
        if ((allHistoriesOut != null && allHistoriesOut.size() > 0) ||
                allHistoriesIn != null && allHistoriesIn.size() > 0) {
            showHistories(allHistoriesOut, allHistoriesIn);
            label:
            while (true) {
                System.out.println("Введите " + Utils.ANSI_RED + "PRINT " + Utils.ANSI_RESET +
                        "для отправки истории в файл." + Utils.ANSI_RED + " EXIT" + Utils.ANSI_RESET + " - для перехода к чату");
                String historyCommand = cons.readLine();
                switch (historyCommand.toUpperCase()) {
                    case "PRINT":
                        ExportMessagesToFile printJsonToFile = new ExportMessagesToFile(nikc, allHistoriesIn, allHistoriesOut);
                        printJsonToFile.writeToJSON();
                        break;
                    case "EXIT":
                        break label;
                    default:
                        System.out.println(Utils.ANSI_RED + "НЕВЕРНАЯ КОМАНДА. ВВЕДИТЕ PRINT ИЛИ EXIT" + Utils.ANSI_RESET);
                        break;
                }
            }
        } else {
            System.out.println(Utils.ANSI_RED + "Нет входящих и исходящих сообщений" + Utils.ANSI_RESET);
        }
    }

    protected void handleMessage(char messageType) throws IOException {
        Message message = null;
        String in;
        switch (messageType) {
            case Utils.COMMAND_TOTAL_MESSAGE:
                System.out.println("Вводите сообщения всем пользователям. Для завершения общения введите " + Utils.ANSI_RED +  "'\\'" + Utils.ANSI_RESET);
                message = new Message(nikc, "ВСЕМ");
                break;
            case Utils.COMMAND_PRIVATE_MESSAGE:
                System.out.println("Вводите сообщения для общения с пользователем. Для завершения общения введите " + Utils.ANSI_RED +  "'\\'"+ Utils.ANSI_RESET);
                message = new Message(nikc, receipent);
                break;
        }

        while ((in = cons.readLine()) != null) {
            if (in.length() == 0 || in.charAt(0) == '#')
                continue;
            if (in.charAt(0) == '\\') {
                System.out.println();
                break;
            } else {
                final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                message.setDate(dateFormat.format(date));
                message.setMessage(in);

                if (putMessageToRedis(message))
                    send(messageType + message.getMessage());
                else
                    System.out.println("Ошибка отправки сообщения");
            }
        }
    }

    boolean putMessageToRedis(Message message) {
        try {
            Gson gson = new Gson();
            String json = gson.toJson(message);
            jedis.sadd(nikc + Utils.MSG_SUFFIX_OUT, json);
            jedis.sadd(receipent + Utils.MSG_SUFFIX_IN, json);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private List<Message> getAllHistoriesOut() {
        List historiesOut = new ArrayList<>();
        Gson gson = new Gson();
        Set<String> messHistoriesOut = jedis.smembers(nikc + Utils.MSG_SUFFIX_OUT);
        for (String history : messHistoriesOut) {
            Message message = gson.fromJson(history, Message.class);
            historiesOut.add(message);
        }

        Collections.sort(historiesOut);

        return historiesOut;
    }

    private List<Message> getAllHistoriesIn() {
        List historiesIn = new ArrayList<>();
        Gson gson = new Gson();
        Set<String> messHistoriesIn = jedis.smembers(nikc + Utils.MSG_SUFFIX_IN);
        for (String history : messHistoriesIn) {
            Message message = gson.fromJson(history, Message.class);
            historiesIn.add(message);
        }

        Collections.sort(historiesIn);

        return historiesIn;
    }

    private void showHistories(List<Message> allHistoriesOut, List<Message> allHistoriesIn) {
        System.out.println("*****************************************************");
        System.out.println("\t\t\tИСТОРИЯ ИСХОДЯЩИХ СООБЩЕНИЙ ДЛЯ ПОЛЬЗОВАТЕЛЯ " + nikc + ":");
        for (Message anAllHistoriesOut : allHistoriesOut) {
            System.out.println("дата - " + anAllHistoriesOut.getDate() + ", кому - " + anAllHistoriesOut.getReceipent() +
                    ", сообщение - " + anAllHistoriesOut.getMessage());
        }

        System.out.println();

        System.out.println("\t\t\tИСТОРИЯ ВХОДЯЩИХ СООБЩЕНИЙ ДЛЯ ПОЛЬЗОВАТЕЛЯ " + nikc + ":");
        for (Message anAllHistoriesIn : allHistoriesIn) {
            System.out.println("дата - " + anAllHistoriesIn.getDate() + ", от кого - " + anAllHistoriesIn.getSender() +
                    ", сообщение - " + anAllHistoriesIn.getMessage());
        }
        System.out.println("*****************************************************");
    }

    private boolean checkIfHasUser(Users curUser) {
        showAllUsers();
        return jedis.sismember(Utils.REDIS_USER_LIST, curUser.getNickName());
    }

    private void addNewUser(Users curUser) {
        jedis.sadd(Utils.REDIS_USER_LIST, curUser.getNickName());
        System.out.println("Добавлен новый пользователь " + curUser.getNickName());
    }

    private void showAllUsers() {
        try {
            System.out.println("---------------------------------------------------------------- ");
            Set<String> allNicknames = jedis.smembers(Utils.REDIS_USER_LIST);
            System.out.println("\t\t\tВсе пользователи чата: ");
            int count = 0;
            if(allNicknames.size() == 0){
                System.out.println("Пользователей не обнаружено");
            }
            for (String nickname : allNicknames) {
                count++;
                System.out.println(Integer.toString(count) + ". " + nickname);
            }
            System.out.println("---------------------------------------------------------------- ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void send(String s) {
        pw.println(s);
        pw.flush();
    }

}

