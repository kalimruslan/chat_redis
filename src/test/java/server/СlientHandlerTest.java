package server;

import models.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by 1 on 16.07.2017.
 */
public class СlientHandlerTest {
    private String userSentText;
    private List<ClientHandler> clientList;

    @Before
    public void setUp() {
        clientList = new ArrayList();
        for (int i = 0; i < 10; i++) {
            ClientHandler handler = new ClientHandler();
            handler.setLogin("login" + i);
            clientList.add(handler);
        }
        assertNotNull(clientList);
    }

    @After
    public void tearDown() throws Exception {
        clientList = null;
        assertNull(clientList);
    }


    @Test
    public void testGetCommandForAuth() {
        userSentText = Utils.COMMAND_AUTH + "test_user";
        userSentText = userSentText.substring(1);

        assertTrue("Логин не верный(дольше 20 символов)", Utils.isValidLoginName(userSentText));
    }

    @Test
    public void testFindClientForPrivateMessage() throws IOException {
        String nick = "login5";
        boolean isFind = false;

        for (ClientHandler aClientList : clientList) {
            ClientHandler сlientHandler = aClientList;
            if (сlientHandler.getLogin().equals(nick))
                isFind = true;
        }
        assertTrue(isFind);
    }

    @Test
    public void testGetCommandForPrivateMessage() {
        String nick = "login5";
        ClientHandler client = null;

        userSentText = Utils.COMMAND_PRIVATE_MESSAGE + "hello";
        userSentText = userSentText.substring(1);

        for (ClientHandler aClientList : clientList) {
            ClientHandler сlientHandler = aClientList;
            if (сlientHandler.getLogin().equals(nick))
                client = сlientHandler;

            if (client == null)
                fail("Клиент не авторизован");
            else
                assertNotNull(client);
        }

    }
}