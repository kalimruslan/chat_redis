package client;

import com.google.gson.Gson;
import models.Config;
import models.Message;
import models.Users;
import models.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by 1 on 14.07.2017.
 */
public class ClientsTest {
    private static Config settings;
    private Jedis jedis;
    private Users users;
    private Clients clients;
    private Message message;

    @Before
    public void setUp() throws Exception {
        settings = Config.createInstance();
        Config.readConfigFile(settings);
        clients = new Clients();
        jedis = new Jedis(settings.getJedisServerHost(), Integer.parseInt(settings.getJedisServerPost()));
        users = new Users("ruslan");
        message = new Message("test_user_from", "test_user_to", "hi, test_user_to");

        assertNotNull(jedis);
        assertNotNull(users);
        assertNotNull(message);
        assertNotNull(settings);
        assertNotNull(clients);

    }

    @After
    public void tearDown() throws Exception {
        settings = null;
        jedis = null;
        users = null;
        message = null;
        clients = null;

        assertNull(settings);
        assertNull(jedis);
        assertNull(users);
        assertNull(message);
        assertNull(clients);
    }

    @Test
    public void testPutMessageToRedis() throws Exception {
        assertTrue("Сообщение не закэшировано", clients.putMessageToRedis(message));
    }

    @Test
    public void testGetAllHistoriesOut() throws Exception {
        List historiesOut = new ArrayList<>();
        Gson gson = new Gson();
        Set<String> messHistoriesOut = jedis.smembers(users.getNickName() + Utils.MSG_SUFFIX_OUT);
        for (String history : messHistoriesOut) {
            Message message = gson.fromJson(history, Message.class);
            historiesOut.add(message);
        }

        assertNotNull(historiesOut);
        assertFalse("Возможно нет исходящих сообщений на Редис", historiesOut.isEmpty());
    }

    @Test
    public void testGetAllHistoriesIn() throws Exception {
        List historiesIn = new ArrayList<>();
        Gson gson = new Gson();
        Set<String> messHistoriesIn = jedis.smembers(users.getNickName() + Utils.MSG_SUFFIX_IN);
        for (String history : messHistoriesIn) {
            Message message = gson.fromJson(history, Message.class);
            historiesIn.add(message);
        }

        assertNotNull(historiesIn);
        assertFalse("возможно нет входящих сообщений на Редис", historiesIn.isEmpty());
    }

    @Test
    public void testCheckIfHasUser() throws Exception {
        boolean exists = jedis.sismember(Utils.REDIS_USER_LIST, users.getNickName());

        assertTrue("Возможно нет такого пользователя на Редис",
                exists);
    }

    @Test
    public void testShowAllUsers() throws Exception {
        Set<String> allNicknames = jedis.smembers(Utils.REDIS_USER_LIST);
        assertFalse("Возможно нет пользоваетлей на Редис", allNicknames.isEmpty());

        int count = 0;
        for (String nickname : allNicknames) {
            count++;
            String expected = Integer.toString(count) + ". " + nickname;
            assertEquals(expected, Integer.toString(count) + ". " + nickname);
        }
    }

}